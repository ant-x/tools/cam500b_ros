#!/usr/bin/env python
import rospy
import cv2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image, CameraInfo

class Cam500B:

    TOPIC_OUT_CAMERA_INFO = 'cam500b/camera_info';
    TOPIC_OUT_IMAGE_RAW = 'cam500b/image_raw';

    def __init__(self, device_index, camera_frame, frame_rate, width, height, orientation):
        self.device_index = device_index
        self.camera_frame = camera_frame
        self.frame_rate = frame_rate
        self.resolution_width = width
        self.resolution_height = height
        self.orientation = orientation

        self.bridge = CvBridge()
        self.info = CameraInfo()
        self.camera = cv2.VideoCapture(self.device_index,cv2.CAP_V4L)
        self.camera.set(3,self.resolution_width)
        self.camera.set(4,self.resolution_height)

        # subscribe to topics

        # register publishers
        self.pub_image_raw = rospy.Publisher(self.TOPIC_OUT_IMAGE_RAW, Image, queue_size=1)
        self.pub_camera_info = rospy.Publisher(self.TOPIC_OUT_CAMERA_INFO, CameraInfo, queue_size=1)

        rospy.loginfo(rospy.get_name() + ' Node initialised')

    def read_image(self):
        self.frame_id = camera_frame
        ret, frame = self.camera.read()
        if ret:
            center = (self.resolution_width / 2, self.resolution_height / 2)
            scale = 1.0
            M = cv2.getRotationMatrix2D(center, self.orientation, scale)
            self.img = cv2.warpAffine(frame, M, (self.resolution_width, self.resolution_height))

    def publish_image(self):
        self.imgmsg = self.bridge.cv2_to_imgmsg(self.img, encoding="passthrough")
        self.imgmsg.header.frame_id = self.camera_frame
        self.imgmsg.header.stamp = rospy.Time.now()
        self.pub_image_raw.publish(self.imgmsg)

    def publish_camera_info(self):
        self.info.header.stamp.secs = self.imgmsg.header.stamp.secs
        self.info.header.stamp.nsecs = self.imgmsg.header.stamp.nsecs
        self.info.height = self.resolution_height
        self.info.width = self.resolution_width
        self.info.header.frame_id = self.camera_frame
        self.pub_camera_info.publish(self.info)

    def run(self):
        rate = rospy.Rate(self.frame_rate)
        while not rospy.is_shutdown():
            self.read_image()
            self.publish_image()
            self.publish_camera_info()

            rate.sleep()

if __name__ == '__main__':
    try:
        rospy.init_node('cam500b', anonymous=True)

        rospy.loginfo(rospy.get_name() + ' Getting parameters...')
        device_index = rospy.get_param('~device_index')
        camera_frame = rospy.get_param('~camera_frame')
        frame_rate = rospy.get_param('~frame_rate')
        width = rospy.get_param('~width')
        height = rospy.get_param('~height')
        orientation = rospy.get_param('~orientation')
        rospy.loginfo(rospy.get_name() + ' done!')

        cam = Cam500B(device_index, camera_frame, frame_rate, width, height, orientation)

        cam.run()

    except rospy.ROSInterruptException:
        pass
