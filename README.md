# CAM500B_ROS

A ROS node to receive information from the FriendlyARM CAM500B and publish it as a ROS topic.

## Setup
Create a catkin workspace:

```
sudo apt-get update
sudo apt-get install ros-noetic-cv-bridge
cd $HOME
mkdir -p catkin_ws_cam500b/src
cd catkin_ws_cam500b/src
git clone https://gitlab.com/ant-x/tools/cam500b_ros.git
cd $HOME/catkin_ws_cam500b
catkin_make
```

setup .bashrc

```
cd $HOME
nano .bashrc
```

add the following lines

```
source $HOME/catkin_ws_cam500b/devel/setup.bash
export ROS_MASTER_URI=http://localhost:11311/
export ROS_HOSTNAME=localhost
```

source .bashrc:

```
source .bashrc
```

Install ``v4l-utils`` utility tool:

```
apt-get install v4l-utils
```

To display information on the camera driver:

```
v4l2-ctl -d /dev/video0 -D
```


## Usage instructions

`roslaunch cam500b cam500b.launch`

## Launch file setup

```
roscd cam500b
nano launch/cam500b.launch
```

## Parameters

* ``device_index`` camera device index (default 0, /dev/video0)
* ``camera_link_name`` camera link name (default cam500b_camera_link)
* ``frame_rate`` frame rate of the camera in Hz (default 1)
* ``width`` camera resolution width (default 1280)
* ``height`` camera resolution height (default 720)
* ``orientation`` frame orientation, positive clockwise (default 0)


## ROS topics

* `cam500b/image_raw` (*sensor_msgs/Image*) http://docs.ros.org/en/api/sensor_msgs/html/msg/Image.html
* `cam500b/camera_info` (*sensor_msgs/CameraInfo*) http://docs.ros.org/en/api/sensor_msgs/html/msg/CameraInfo.html

## References
https://wiki.friendlyelec.com/wiki/index.php/NanoPi_NEO_Air#Connect_to_DVP_Camera_CAM500B

https://wiki.friendlyelec.com/wiki/index.php/Matrix_-_CAM500B

further information: take inspiration from the openmv-cam node developed by Gabriele Roggi, POLIMI: 

https://gitlab.com/flyart/openmv-cam
